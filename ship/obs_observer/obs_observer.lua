pollNum = {}
pollTyp = {}
pollTotal = 0
pollPTotal = {}

function Poll ()
  SobGroup_CreateIfNotExist ("temp")

  pollTotal = 0
  pollPTotal = {}

  for player = 0, Universe_PlayerCount ()- 1 do
    local num, int = SobGroup_Count ("Player_Ships"..player), 0

    pollNum[player + 1]= {}
    pollTyp = {}
    pollPTotal[player + 1]= num

    if num > 0 then
      while int < num do
        SobGroup_FillShipsByIndexRange ("temp", "Player_Ships"..player,int,1)

        local cnt, typ = SobGroup_Count("temp"), SobGroup_GetShipType ("temp") or "Unknown"

        if (not pollNum[player+1][typ]) then
          pollNum[player+1][typ]= 0
        end
        if (not pollTyp[typ]) then
          pollTyp[typ]= 0
        end

        pollNum[player+1][typ]= pollNum[player+1][typ]+ cnt
        pollTyp[typ]= pollTyp[typ]+ cnt

        if cnt == 0 then int = int + 1 else int = int + cnt end
      end
    end
  end
end

function PrintPoll ()
  for k,v in pollTyp do
    print (k.." "..v)
  end

  print ("Total "..pollTotal)

  for i = 1, Universe_PlayerCount() do
    print ("Player"..(i-1).." Total "..pollPTotal[i])

    for k,v in pollNum[i] do
      print ("Player"..(i-1).." "..k.." "..v)
    end
  end
end