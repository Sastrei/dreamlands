Path_Name = "Observer"
Symbolic_Name = "Observer"
Display_Name = "Spectator"
Prefix = "OBS_"

ExtFilter = "races_gbx,races_obs,races_hwrm,races_dm,races_gbx_hwrmc"
Tags = "race_gbx,race_obs,race_hwrm,race_dm,race_gbx_hwrmc"

SG_Pass_Tags = "sgf_common,sgf_kus"
